﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGF.Game
{ 
    public class GameAudio : MonoBehaviour
    {
        static GameAudio _inst;
        static GameAudio instance
        {
            get
            {
                if (null == _inst)
                {
                    GameObject root = new GameObject("GameAudio");
                    DontDestroyOnLoad(root);
                    _inst = root.AddComponent<GameAudio>();
                }
                return _inst;
            }

            set
            {
                _inst = value;
            }
        }

        static AudioSource _music;
        static public AudioSource music
        {
            get
            {
                if (null == _music)
                {
                    _music = instance.gameObject.AddComponent<AudioSource>();
                    _music.playOnAwake = false;
                    _music.loop = true;
                }
                return _music;
            }
        }

        private void Awake()
        {
            if (null == _inst)
            {
                _inst = this;
                DontDestroyOnLoad(gameObject);
            }

            bgms.RemoveAll(x => x == null);

            InvokeRepeating("PlayBGM", 0.0f, 0.5f);
        }

        void PlayBGM()
        {
            if (music.isPlaying || music.mute)
            {
                return;
            }
            AudioClip bgm = bgms[Random.Range(0, bgms.Count)];
            music.PlayOneShot(bgm);
        }

        public static void PlayPopSFX(int id)
        {
            try
            {
                sound.PlayOneShot(instance. popSfx[id], 0.5f);
            }
            catch { }
        }
        public class SoundList
        {
            List<AudioSource> sourceList = new List<AudioSource>();
            public AudioSource Get()
            {
                AudioSource source = sourceList.Find(x => null != x && !x.isPlaying);
                if (null == source)
                {
                    source = instance.gameObject.AddComponent<AudioSource>();
                    source.playOnAwake = false;
                    source.loop = false;
                    source.mute = mute;
                    sourceList.Add(source);
                }
                return source;
            }

            public AudioSource PlayOneShot(AudioClip clip, float volume = 1.0f)
            {
                if (null == clip)
                {
                    return null;
                }
                var source = Get();
                source.volume = volume;
                source.clip = clip;
                source.loop = false;
                source.Play();
                return source;
            }

            bool _mute = false;
            public bool mute
            {
                set
                {
                    _mute = value;
                    foreach (AudioSource source in sourceList)
                    {
                        source.mute = value;
                    }
                }
                get
                {
                    return _mute;
                }
            }
        }

        static SoundList _sound = new SoundList();
        static public SoundList sound
        {
            get
            {
                return _sound;
            }
        }

        [SerializeField]
        List<AudioClip> bgms = new List<AudioClip>();

        [SerializeField]
        List<AudioClip> popSfx = new List<AudioClip>();
    }
}