﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGF.Game.Assets
{
    [System.Serializable]
    public class TileAssets<T> : ScriptableObject where T : UnityEngine.Object
    {
        [SerializeField]
        protected List<T> assets = new List<T>();

        public int Count
        {
            get
            {
                return assets.Count;
            }
        }

        public T GetById(int id)
        {
            try
            {
                return assets[id];
            }
            catch { }
            return null;
        }

        public List<int> GetTileTypeSet()
        {
            List<int> list = new List<int>();
            for (int i = 0; i < assets.Count; i++)
            {
                list.Add(i);
            }
            return list;
        }
    }
}
