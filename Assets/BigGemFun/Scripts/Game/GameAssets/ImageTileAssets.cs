﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BGF.Game.Assets
{
    [CreateAssetMenu(fileName = "ImageTileAssets", menuName = "BGF/ImageTileAssets", order = 99)]
    [System.Serializable]
    public class ImageTileAssets : TileAssets<Sprite>
    {

    }
}