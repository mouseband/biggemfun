﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BGF.UI;
using BGF.UI.Views;

namespace BGF
{
    public static class BGFGame
    {
        public static bool pause = false;

        public static MainGameUI mainGameUI = null;

        public static GridView gridView = null;

        public static int level = 0;

        public static int moves { get; private set; }
        public static int totalMoves { get; private set; }

        public static float time { get; private set; }
        public static float totalTime { get; private set; }

        public static bool enableTime = false;
        public static bool enableMoves = false;

        public static bool swipeMode = true;

        public static List<int> tileCount = new List<int>();

        public static string tileSetAssetsName = "TileSets/GemStones";


        public static void RestartGame()
        {
            level = 1;
            StartLevel();
        }

        public static void StartLevel()
        {
            InitLevel(level);
            if (null != mainGameUI)
            {
                mainGameUI.InitLevel(level);
            }
            if (null != gridView)
            {
                gridView.viewModel.ResetGame();
            }
        }

        static void InitLevel(int aNewLevel)
        {
            moves = 9 + aNewLevel * 18;
            totalMoves = moves;
            time = 50.0f + aNewLevel * 10.0f;
            totalTime = time;

            int tileCountToCollect = 3 + (aNewLevel - 1) * 2;
            tileCount.Clear();
            for (int i = 0; i < gridView.viewModel.gameBoard.tileGenerator.tileTypeSet.Count; i++)
            {
                tileCount.Add(tileCountToCollect);
            }
        }

        public static bool CheckGameWin()
        {
            foreach (int count in tileCount)
            {
                if (0 < count)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool CheckGameLose()
        {
            if (0 >= moves || 0.0f >= time)
            {
                foreach (int count in tileCount)
                {
                    if (0 < count)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static void OnUpdate()
        {
            if (enableTime && !pause)
            {
                time -= Time.deltaTime;
            }

            if (CheckGameLose())
            {
                mainGameUI.GameOver();
            }
        }

        public static void OnMove()
        {
            if (enableMoves)
            {
                --moves;
            }
        }

        public static void OnPopTiles(List<BGF.Core.TileData> tiles)
        {
            foreach (var data in tiles)
            {
                try
                {
                    tileCount[data.typeId]--;
                    tileCount[data.typeId] = Mathf.Max(tileCount[data.typeId], 0);
                    if (enableTime)
                    {
                        time += 0.5f;
                    }
                }
                catch { }
            }

            moves += Mathf.RoundToInt(tiles.Count / 6.0f);

            if (CheckGameWin())
            {
                level++;
                StartLevel();
            }
        }
    }
}