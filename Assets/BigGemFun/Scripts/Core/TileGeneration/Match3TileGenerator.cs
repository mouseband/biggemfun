﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BGF.Core.TileGeneration
{
    public abstract class Match3TileGernerator
    {
        public abstract List<TileData> Fill(Match3TileGrid grid);

        public HashSet<int> tileTypeSet { get; protected set; }

        public Match3TileGernerator()
        {
            tileTypeSet = new HashSet<int>();
        }

        public void SetTileTypes(IEnumerable<int> aNewTileTypeSet)
        {
            tileTypeSet.Clear();
            if (null == aNewTileTypeSet)
            {
                return;
            }
            foreach (int type in aNewTileTypeSet)
            {
                if (!tileTypeSet.Contains(type))
                {
                    tileTypeSet.Add(type);
                }
            }
        }

        public void AddTileType(int aNewTileType)
        {
            if (tileTypeSet.Contains(aNewTileType))
            {
                return;
            }
            tileTypeSet.Add(aNewTileType);
        }

        public void RemoveTileType(int aTileType)
        {
            if (!tileTypeSet.Contains(aTileType))
            {
                return;
            }
            tileTypeSet.Remove(aTileType);
        }

        public int GetRandomTileType()
        {
            try
            {
                return tileTypeSet.ToArray()[Random.Range(0, tileTypeSet.Count)];
            }
            catch
            {
                return Constants.INVALID_ID;
            }
        }

        public int GetRandomTileTypeExcept(IEnumerable<int> exceptions)
        {
            try
            {
                HashSet<int> newSet = new HashSet<int>(tileTypeSet);
                newSet.ExceptWith(exceptions);
                return newSet.ToArray()[Random.Range(0, newSet.Count)];
            }
            catch
            {
                return Constants.INVALID_ID;
            }
        }
    }
}
