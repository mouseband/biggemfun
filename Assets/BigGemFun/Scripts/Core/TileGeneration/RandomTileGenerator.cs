﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGF.Core.TileGeneration
{
    public class RandomTileGenerator : Match3TileGernerator
    {
        public override List<TileData> Fill(Match3TileGrid grid)
        {
            List<TileData> addedTiles = new List<TileData>();

            if (null == grid)
            {
                return addedTiles;
            }

            // add from left to right (1st), bottom to top (2nd).
            for (int y = grid.size.y - 1; y >= 0; y--)
            {
                for (int x = 0; x < grid.size.x; x++)
                {
                    Match3Tile tile = grid.GetTileByCellPos(x, y);
                    if (null == tile)
                    {
                        continue;
                    }

                    // Skip tile if alread has a type
                    if (Constants.INVALID_ID == tile.typeId)
                    {
                        // Get random type for new tile
                        int newType = GetRandomTileType();

                        // Test does new type make match
                        if (grid.TestMatch3(newType, x, y))
                        {
                            // Get a new type not same as left or bottom tile
                            newType = GetRandomTileTypeExcept(new int[] { grid.GetTileTypeByCellPos(x - 1, y), grid.GetTileTypeByCellPos(x, y + 1) });
                        }
                        tile.typeId = newType;

                        tile.pos = new Vector2Int(x, y);

                        addedTiles.Add(tile.ToTileData());
                    }
                }
            }
            return addedTiles;
        }
    }
}