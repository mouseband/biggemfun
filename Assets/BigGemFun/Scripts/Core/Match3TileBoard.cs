﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BGF.Core.TileGeneration;

namespace BGF.Core
{
    public class Match3TileBoard
    {
        public const string ACT_ADD_TILES_KEY = "ACT_ADD_TILES_KEY";
        public const string ACT_MOVE_TILES_KEY = "ACT_MOVE_TILES_KEY";
        public const string ACT_POP_TILES_KEY = "ACT_POP_TILES_KEY";
        public const string ACT_SHUFFLE_KEY = "ACT_SHUFFLE_KEY";
        public const string ACT_SHOW_TIP_KEY = "ACT_SHOW_TIP_KEY";
        public const string ACT_MOVE_COUNT_KEY = "ACT_MOVE_COUNT_KEY";

        public Match3TileGrid grid { get; protected set; }

        public Match3TileGernerator tileGenerator { get; protected set; }
        IEnumerable<int> tileTypeSet;

        public Vector2Int size
        {
            get
            {
                if (null == grid)
                {
                    return new Vector2Int();
                }
                return grid.size;
            }
        }

        #region Batch Actions
        public Match3TileMessagePackage SwitchTiles(Vector2Int fromCellPos, Vector2Int toCellPos)
        {
            Match3TileMessagePackage package = new Match3TileMessagePackage();

            // Switch tiles
            grid.SwitchTilesByCellPos(fromCellPos.x, fromCellPos.y, toCellPos.x, toCellPos.y);

            // Test matches
            bool isMatch = grid.TestMatch3(fromCellPos.x, fromCellPos.y) || grid.TestMatch3(toCellPos.x, toCellPos.y);
            // No matches, switch back
            if (!isMatch)
            {
                List<TileData> tiles = grid.SwitchTilesByCellPos(fromCellPos.x, fromCellPos.y, toCellPos.x, toCellPos.y);
                package.AddMessage(new Match3TileMessage(ACT_MOVE_TILES_KEY, tiles));
                return package;
            }
            package.AddMessage(new Match3TileMessage(ACT_MOVE_COUNT_KEY));
            // Pop matches 
            package.AddPackage(MatchPopAndFill());
            return package;
        }

        public Match3TileMessagePackage MatchPopAndFill()
        {
            Match3TileMessagePackage package = new Match3TileMessagePackage();

            // Test matches
            List<TileData> matches = grid.GetAllMatches();

            while (0 < matches.Count)
            {
                // Pop all matches
                package.AddMessage(PopTiles(matches));

                // Collapse existing tiles and create empty spaces at grid top
                var collapseDownMsg = CollapseDown();
                // Create new tiles to fill spaces at grid top
                var fillMsg = FillBoard();
                // But giving message to add new tiles first
                package.AddMessage(fillMsg);
                // then move all tiles down
                package.AddMessage(collapseDownMsg);
                matches = grid.GetAllMatches();
            }

            // cannot move
            bool canMatch = 0 < grid.GetAllMatchMoves().Count;
            if (!canMatch)
            {
                package.AddPackage(Shuffle());
            }

            return package;
        }

        public Match3TileMessagePackage Shuffle()
        {
            Match3TileMessagePackage package = new Match3TileMessagePackage();

            // Pop all existing tiles
            package.AddMessage(ShuffleAllTiles());

            // Regenerate new tiles
            Match3TileMessage fillBoardMessage = FillBoard();

            // Make sure contains moves (if no more moves, do shuffle)
            while (0 == grid.GetAllMatchMoves().Count)
            {
                grid.FillWithEmpty();
                fillBoardMessage = FillBoard();
            }
            package.AddMessage(fillBoardMessage);

            // Give a call to move in new tiles
            package.AddMessage(new Match3TileMessage(ACT_MOVE_TILES_KEY));
            return package;
        }

        public Match3TileMessagePackage ShowTips()
        {
            Match3TileMessagePackage package = new Match3TileMessagePackage();
            package.AddMessage(GetMatchMoves());
            return package;
        }
        #endregion Batch Actions

        #region Message Actions
        public Match3TileMessage FillBoard()
        {
            if (null == grid || null == tileGenerator || null == tileTypeSet)
            {
                return null;
            }

            Match3TileMessage message = new Match3TileMessage();
            message.id = ACT_ADD_TILES_KEY;
            message.data = tileGenerator.Fill(grid);

            return message;
        }

        public Match3TileMessage CollapseDown()
        {
            if (null == grid)
            {
                return null;
            }

            Match3TileMessage message = new Match3TileMessage();
            message.id = ACT_MOVE_TILES_KEY;
            message.data = grid.Collapse_Down();
            return message;
        }

        public Match3TileMessage PopTiles(List<TileData> tiles)
        {
            if (null == tiles || 0 == tiles.Count)
            {
                return null;
            }

            // Set poped tile type to null
            foreach (TileData tileData in tiles)
            {
                grid.SetTileTypeByCellPos(Constants.INVALID_ID, tileData.cellPos.x, tileData.cellPos.y);
            }

            // Create message
            Match3TileMessage message = new Match3TileMessage();
            message.id = ACT_POP_TILES_KEY;
            message.data = tiles;
            return message;
        }

        public Match3TileMessage ShuffleAllTiles()
        {
            if (null == grid || null == grid.tiles || 0 == grid.tiles.Count)
            {
                return null;
            }

            // Get all tiles
            List<TileData> messageData = new List<TileData>();
            foreach (Match3Tile tile in grid.tiles)
            {
                if (null == tile)
                {
                    continue;
                }
                messageData.Add(tile.ToTileData());
            }

            // Empty grid
            grid.FillWithEmpty();

            // Create message 
            Match3TileMessage message = new Match3TileMessage();
            message.id = ACT_SHUFFLE_KEY;
            message.data = messageData;

            return message;
        }

        public Match3TileMessage GetMatchMoves()
        {
            return new Match3TileMessage(ACT_SHOW_TIP_KEY, grid.GetAllMatchMoves());
        }

        #endregion Message Actions

        #region Init
        public void InitBoard(int sizeX, int sizeY, Match3TileGernerator aNewGenerator, IEnumerable<int> aNewTileTypeSet)
        {
            SetSize(sizeX, sizeY);
            SetGenerator(aNewGenerator);
            SetTileTypes(aNewTileTypeSet);
        }

        public void SetSize(int sizeX, int sizeY)
        {
            grid = new Match3TileGrid(sizeX, sizeY);
        }

        public void SetGenerator(Match3TileGernerator aNewGenerator)
        {
            tileGenerator = aNewGenerator;
            SetupTileType();
        }

        public void SetTileTypes(IEnumerable<int> aNewTileTypeSet)
        {
            tileTypeSet = aNewTileTypeSet;
            SetupTileType();
        }

        void SetupTileType()
        {
            if (null != tileGenerator && null != tileTypeSet)
            {
                tileGenerator.SetTileTypes(tileTypeSet);
            }
        }
        #endregion Init
    }
}