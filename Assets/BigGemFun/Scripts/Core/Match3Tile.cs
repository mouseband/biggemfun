﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGF.Core
{
    public class TileData
    {
        public int id = Constants.INVALID_ID;
        public int typeId = Constants.INVALID_ID;
        public Vector2Int cellPos = new Vector2Int();

        public TileData() { }
        public TileData(int aNewId, int aNewType, Vector2Int aNewPos)
        {
            id = aNewId;
            typeId = aNewType;
            cellPos = aNewPos;
        }
    }

    public class Match3Tile
    {
        public int id { get; protected set; }
        public int typeId = Constants.INVALID_ID;
        public Vector2Int pos = new Vector2Int();

        static int currentId = Constants.INVALID_ID;
        static int GetId()
        {
            ++currentId;
            if (currentId > 999999)
            {
                currentId = 0;
            }
            return currentId;
        }

        public Match3Tile()
        {
            id = GetId();
        }

        public Match3Tile(int aNewType)
        {
            id = GetId();
            typeId = aNewType;
        }

        public Match3Tile(int posX, int posY)
        {
            id = GetId();
            pos = new Vector2Int(posX, posY);
        }

        public Match3Tile(int aNewType, int posX, int posY)
        {
            id = GetId();
            typeId = aNewType;
            pos = new Vector2Int(posX, posY);
        }

        public TileData ToTileData()
        {
            TileData data = new TileData();
            data.id = this.id;
            data.typeId = this.typeId;

            data.cellPos = this.pos;
            return data;
        }
    }
}
