﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGF.Core
{
    public class Match3TileGrid
    {
        public const int MIN_SIZE = 1;

        public Vector2Int size { get; protected set; }
        public List<Match3Tile> tiles { get; protected set; }

        #region Construction & Init
        public Match3TileGrid(int sizeX, int sizeY)
        {
            tiles = new List<Match3Tile>();
            size = new Vector2Int(Mathf.Max(sizeX, MIN_SIZE), Mathf.Max(sizeY, MIN_SIZE));
            FillWithEmpty();
        }

        public void FillWithEmpty()
        {
            tiles.Clear();
            for (int y = 0; y < size.y; y++)
            {
                for (int x = 0; x < size.x; x++)
                {
                    tiles.Add(new Match3Tile(x, y));
                }
            }
        }
        #endregion Construction & Init

        #region Match3 Actions
        public bool IsSameTileType(int tileTypeId, int posX, int posY)
        {
            if (Constants.INVALID_ID == tileTypeId)
            {
                return false;
            }
            return GetTileTypeByCellPos(posX, posY) == tileTypeId;
        }

        public bool TestMatch3(int posX, int posY)
        {
            Match3Tile tile = GetTileByCellPos(posX, posY);
            if (null == tile || Constants.INVALID_ID == tile.typeId)
            {
                return false;
            }

            return TestMatch3(tile.typeId, posX, posY);
        }

        public bool TestMatch3(int tileType, int posX, int posY)
        {
            // Test start from Left
            if (IsSameTileType(tileType, posX - 1, posY))
            {
                // xxo
                if (IsSameTileType(tileType, posX - 2, posY))
                {
                    return true;
                }
                // xox
                if (IsSameTileType(tileType, posX + 1, posY))
                {
                    return true;
                }
            }

            // Top
            if (IsSameTileType(tileType, posX, posY - 1))
            {
                // x
                // x
                // o
                if (IsSameTileType(tileType, posX, posY - 2))
                {
                    return true;
                }

                // x
                // o
                // x
                if (IsSameTileType(tileType, posX, posY + 1))
                {
                    return true;
                }
            }

            // right oxx
            if (IsSameTileType(tileType, posX + 1, posY) && IsSameTileType(tileType, posX + 2, posY))
            {
                return true;
            }

            // bottom
            // o
            // x
            // x
            if (IsSameTileType(tileType, posX, posY + 1) && IsSameTileType(tileType, posX, posY + 2))
            {
                return true;
            }

            return false;
        }

        public List<TileData> GetAllMatches()
        {
            List<TileData> matchedTiles = new List<TileData>();
            HashSet<int> addedTileIds = new HashSet<int>();
            for (int y = 0; y < size.y; y++)
            {
                for (int x = 0; x < size.x; x++)
                {
                    Match3Tile tile = GetTileByCellPos(x, y);
                    if (null == tile || Constants.INVALID_ID == tile.typeId)
                    {
                        continue;
                    }

                    // oxx
                    int matchCountHor = 0;
                    while (IsSameTileType(tile.typeId, x + 1 + matchCountHor, y))
                    {
                        ++matchCountHor;
                    }

                    if (matchCountHor > 1)
                    {
                        for (int i = 1; i <= matchCountHor; i++)
                        {
                            int cellId = GetCellIndexByPos(x + i, y);
                            if (!addedTileIds.Contains(cellId))
                            {
                                addedTileIds.Add(cellId);
                                matchedTiles.Add(GetTileByCellIndex(cellId).ToTileData());
                            }
                        }
                    }

                    // o
                    // x
                    // x
                    int matchCountVer = 0;
                    while (IsSameTileType(tile.typeId, x, y + 1 + matchCountVer))
                    {
                        ++matchCountVer;
                    }

                    if (matchCountVer > 1)
                    {
                        for (int i = 1; i <= matchCountVer; i++)
                        {
                            int cellId = GetCellIndexByPos(x, y + i);
                            if (!addedTileIds.Contains(cellId))
                            {
                                addedTileIds.Add(cellId);
                                matchedTiles.Add(GetTileByCellIndex(cellId).ToTileData());
                            }
                        }
                    }

                    // if match, add the test tile
                    if (matchCountHor > 1 || matchCountVer > 1)
                    {
                        int cellId = GetCellIndexByPos(x, y);
                        if (!addedTileIds.Contains(cellId))
                        {
                            addedTileIds.Add(cellId);
                            matchedTiles.Add(GetTileByCellIndex(cellId).ToTileData());
                        }
                    }
                }
            }

            return matchedTiles;
        }

        public List<TileData> GetAllMatchMoves()
        {
            List<TileData> matchedTiles = new List<TileData>();
            for (int y = 0; y < size.y; y++)
            {
                for (int x = 0; x < size.x; x++)
                {
                    Match3Tile tile = GetTileByCellPos(x, y);
                    if (null == tile || Constants.INVALID_ID == tile.typeId)
                    {
                        continue;
                    }

                    // ox
                    if (IsSameTileType(tile.typeId, x + 1, y))
                    {
                        // x--
                        // -ox
                        if (IsSameTileType(tile.typeId, x - 1, y - 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x - 1, y - 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x - 1, y).ToTileData());
                        }

                        // -ox
                        // x--
                        if (IsSameTileType(tile.typeId, x - 1, y + 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x - 1, y + 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x - 1, y).ToTileData());
                        }
                        // x-ox
                        if (IsSameTileType(tile.typeId, x - 2, y))
                        {
                            matchedTiles.Add(GetTileByCellPos(x - 2, y).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x - 1, y).ToTileData());
                        }


                        // --x
                        // ox-
                        if (IsSameTileType(tile.typeId, x + 2, y - 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x + 2, y - 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x + 2, y).ToTileData());
                        }
                        // ox-
                        // --x
                        if (IsSameTileType(tile.typeId, x + 2, y + 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x + 2, y + 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x + 2, y).ToTileData());
                        }
                        // ox-x
                        if (IsSameTileType(tile.typeId, x + 3, y))
                        {
                            matchedTiles.Add(GetTileByCellPos(x + 3, y).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x + 2, y).ToTileData());
                        }
                    }

                    // o
                    // x
                    if (IsSameTileType(tile.typeId, x, y + 1))
                    {
                        // o-
                        // x-
                        // -x
                        if (IsSameTileType(tile.typeId, x + 1, y + 2))
                        {
                            matchedTiles.Add(GetTileByCellPos(x + 1, y + 2).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x, y + 2).ToTileData());
                        }

                        // -o
                        // -x
                        // x-
                        if (IsSameTileType(tile.typeId, x - 1, y + 2))
                        {
                            matchedTiles.Add(GetTileByCellPos(x - 1, y + 2).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x, y + 2).ToTileData());
                        }
                        // o
                        // x
                        // -
                        // x
                        if (IsSameTileType(tile.typeId, x, y + 3))
                        {
                            matchedTiles.Add(GetTileByCellPos(x, y + 3).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x, y + 2).ToTileData());
                        }

                        // x
                        // -
                        // o
                        // x
                        if (IsSameTileType(tile.typeId, x, y - 2))
                        {
                            matchedTiles.Add(GetTileByCellPos(x, y - 2).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x, y - 1).ToTileData());
                        }

                        // -x
                        // o-
                        // x-
                        if (IsSameTileType(tile.typeId, x + 1, y - 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x + 1, y - 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x, y - 1).ToTileData());
                        }
                        // x-
                        // -o
                        // -x
                        if (IsSameTileType(tile.typeId, x - 1, y - 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x - 1, y - 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x, y - 1).ToTileData());
                        }
                    }

                    // o-x
                    if (IsSameTileType(tile.typeId, x + 2, y))
                    {
                        // -x-
                        // o-x
                        if (IsSameTileType(tile.typeId, x + 1, y - 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x + 1, y - 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x + 1, y).ToTileData());
                        }

                        // o-x
                        // -x-
                        if (IsSameTileType(tile.typeId, x + 1, y + 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x + 1, y + 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x + 1, y).ToTileData());
                        }
                    }

                    // o
                    // -
                    // x
                    if (IsSameTileType(tile.typeId, x, y + 2))
                    {
                        // o-
                        // -x
                        // x-
                        if (IsSameTileType(tile.typeId, x + 1, y + 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x + 1, y + 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x, y + 1).ToTileData());
                        }

                        // -o
                        // x-
                        // -x
                        if (IsSameTileType(tile.typeId, x - 1, y + 1))
                        {
                            matchedTiles.Add(GetTileByCellPos(x - 1, y + 1).ToTileData());
                            matchedTiles.Add(GetTileByCellPos(x, y + 1).ToTileData());
                        }
                    }
                }
            }

            return matchedTiles;
        }

        public List<TileData> SwitchTilesByCellPos(int fromCellPosX, int fromCellPosY, int toCellPosX, int toCellPosY)
        {
            // Get tiles by pos
            Match3Tile fromTile = GetTileByCellPos(fromCellPosX, fromCellPosY);
            Match3Tile toTile = GetTileByCellPos(toCellPosX, toCellPosY);

            List<TileData> movedTiles = new List<TileData>();
            if (null == fromTile || null == toTile)
            {
                Debug.LogErrorFormat("Switch Tile Fail: Tile cannot be null.  {0} ({1}, {2}) <==> {3} ({4}, {5})",
                    (null == fromTile ? "Null" : fromTile.typeId.ToString()), fromCellPosX, fromCellPosY,
                    (null == toTile ? "Null" : toTile.typeId.ToString()), toCellPosX, toCellPosY);
                return movedTiles;
            }

            // Switch tiles
            int cellFromId = GetCellIndexByPos(fromCellPosX, fromCellPosY);
            int cellToId = GetCellIndexByPos(toCellPosX, toCellPosY);

            tiles[cellFromId] = toTile;
            tiles[cellToId] = fromTile;

            // Update tile pos
            fromTile.pos = new Vector2Int(toCellPosX, toCellPosY);
            toTile.pos = new Vector2Int(fromCellPosX, fromCellPosY);


            movedTiles.Add(fromTile.ToTileData());
            movedTiles.Add(toTile.ToTileData());
            return movedTiles;
        }

        public List<TileData> Collapse_Down()
        {
            List<TileData> movedTiles = new List<TileData>();

            for (int x = 0; x < size.x; x++)
            {
                int moveDownSteps = 0;
                for (int y = size.y - 1; y >= 0; y--)
                {
                    var tile = GetTileByCellPos(x, y);

                    if (Constants.INVALID_ID == tile.typeId)
                    {
                        ++moveDownSteps;
                    }
                    else if (0 != moveDownSteps && Constants.INVALID_ID != tile.typeId)
                    {
                        SwitchTilesByCellPos(x, y, x, y + moveDownSteps);
                        movedTiles.Add(tile.ToTileData());
                    }
                }
            }

            return movedTiles;
        }
        #endregion

        #region Tile Getters & Setters
        public Match3Tile GetTile(System.Predicate<Match3Tile> predicate)
        {
            try
            {
                return tiles.Find(predicate);
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                return null;
            }
        }

        public List<Match3Tile> GetTiles(System.Predicate<Match3Tile> predicate)
        {
            try
            {
                return tiles.FindAll(predicate);
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                return new List<Match3Tile>();
            }
        }

        public Match3Tile GetTileByCellIndex(int index)
        {
            try
            {
                return tiles[index];
            }
            catch
            {
                return null;
            }
        }

        protected int GetCellIndexByPos(int posX, int posY)
        {
            if (0 > posX || 0 > posY || posX >= size.x || posY >= size.y)
            {
                return Constants.INVALID_ID;
            }

            return size.x * posY + posX;
        }

        public Match3Tile GetTileByCellPos(int posX, int posY)
        {
            int cellId = GetCellIndexByPos(posX, posY);

            if (Constants.INVALID_ID == cellId)
            {
                return null;
            }

            return GetTileByCellIndex(cellId);
        }

        public int GetTileTypeByCellPos(int posX, int posY)
        {
            Match3Tile tile = GetTileByCellPos(posX, posY);
            return null == tile ? Constants.INVALID_ID : tile.typeId;
        }

        public bool SetTileTypeByCellPos(int aNewTileType, int posX, int posY)
        {
            Match3Tile tile = GetTileByCellPos(posX, posY);
            if (null == tile)
            {
                return false;
            }

            tile.typeId = aNewTileType;
            return true;
        }
        #endregion Tile Getters & Setters
    }
}