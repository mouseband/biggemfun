﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGF.Core
{
    public class Match3TileMessage
    {
        public string id;
        public List<TileData> data = new List<TileData>();

        public Match3TileMessage()
        {
        }

        public Match3TileMessage(string aNewMessageId, List<TileData> aNewTileDataList = null)
        {
            id = aNewMessageId;
            data = aNewTileDataList;
        }
    }

    public class Match3TileMessagePackage
    {
        public int Count
        {
            get
            {
                return messageQueue.Count;
            }
        }

        protected Queue<Match3TileMessage> messageQueue = new Queue<Match3TileMessage>();

        public void AddPackage(Match3TileMessagePackage aNewPackage)
        {
            if (null == aNewPackage)
            {
                return;
            }

            while (0 < aNewPackage.messageQueue.Count)
            {
                messageQueue.Enqueue(aNewPackage.messageQueue.Dequeue());
            }
        }

        public void AddMessage(string aNewMessageId, List<TileData> aNewTileDataList)
        {
            if (string.IsNullOrWhiteSpace(aNewMessageId))
            {
                return;
            }

            AddMessage(new Match3TileMessage(aNewMessageId, aNewTileDataList));
        }

        public void AddMessage(Match3TileMessage aNewMessage)
        {
            if (null == aNewMessage || string.IsNullOrWhiteSpace(aNewMessage.id))
            {
                return;
            }

            messageQueue.Enqueue(aNewMessage);
        }

        public Match3TileMessage DequeueMessage()
        {
            if (0 == messageQueue.Count)
            {
                return null;
            }
            return messageQueue.Dequeue();
        }
    }
}