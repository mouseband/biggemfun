﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;
using BGF.Core;

namespace BGF.Core.UnitTest
{ 
    public class Match3TileGridUnitTest
    {
        [MenuItem("BGF/Unit Test/Core/Match 3 Tile Grid")]
        static void UnitTest()
        {
            Debug.Log("Unit Test - Match 3 Tile Grid");

            AssertNewGrid(-1, -1);
            AssertNewGrid(0, 3);
            AssertNewGrid(5, 5);

            AssertTileTypeSetterGetter();

            AssertMatch3Test(new Match3TileGrid(5, 5), new Vector2Int[]{
                new Vector2Int(1, 2),
                new Vector2Int(2, 2),
                new Vector2Int(3, 2)
            });

            AssertMatch3Test(new Match3TileGrid(5, 5), new Vector2Int[]{
                new Vector2Int(2, 1),
                new Vector2Int(2, 2),
                new Vector2Int(2, 3)
            });

            AssertMatch3Test(new Match3TileGrid(5, 5), new Vector2Int[]{
                new Vector2Int(1, 2),
                new Vector2Int(2, 2),
                new Vector2Int(3, 2),
                new Vector2Int(2, 1),
                new Vector2Int(2, 3),
                new Vector2Int(4, 2)
            });

            TestGetAllMatch();
            TestSwitchTiles();

            Debug.Log("Unit Test End - Match 3 Tile Grid");
        }

        static void AssertNewGrid(int sizeX, int sizeY)
        {
            Match3TileGrid grid = new Match3TileGrid(sizeX, sizeY);
            Assert.IsNotNull(grid, "Create Tile Grid");

            int expectedSizeX = sizeX < Match3TileGrid.MIN_SIZE ? Match3TileGrid.MIN_SIZE : sizeX;
            int expectedSizeY = sizeY < Match3TileGrid.MIN_SIZE ? Match3TileGrid.MIN_SIZE : sizeY;
            Assert.AreEqual<Vector2Int>(new Vector2Int(expectedSizeX, expectedSizeY), grid.size, "Grid Size");

            Assert.AreEqual<int>(expectedSizeX * expectedSizeY, grid.tiles.Count, "Tile Count");

            Assert.AreEqual<int>(expectedSizeX * expectedSizeY, grid.GetTiles(x => null != x).Count, "Tile Instance Count");

            Assert.AreEqual<int>(0, grid.GetTiles(x => null != x && Constants.INVALID_ID != x.typeId).Count, "Tile Init Type");
        }

        static void AssertTileTypeSetterGetter()
        {
            Match3TileGrid grid = new Match3TileGrid(5, 5);
            int type = grid.GetTileTypeByCellPos(2, 2);

            Assert.AreEqual<int>(Constants.INVALID_ID, type);
            int aNewType = 3;
            grid.SetTileTypeByCellPos(aNewType, 2, 2);
            Assert.AreEqual<int>(3, grid.GetTileTypeByCellPos(2, 2));
        }

        static void AssertMatch3Test(Match3TileGrid grid, Vector2Int[] matchPos)
        {
            Assert.IsNotNull(grid, "AssertMatch3Test Grid");
            grid.FillWithEmpty();
            int type = 1;
            Assert.IsNotNull(matchPos, "AssertMatch3Test Post List");
            Assert.IsTrue(3 <= matchPos.Length, "At least 3 tiles to test");
            // Set tiles
            foreach (Vector2Int pos in matchPos)
            {
                grid.SetTileTypeByCellPos(type, pos.x, pos.y);
            }


            for (int y = 0; y < grid.size.y; y++)
            {
                for (int x = 0; x < grid.size.x; x++)
                {
                    Match3Tile tile = grid.GetTileByCellPos(x, y);
                    bool match = grid.TestMatch3(tile.typeId, x, y);
                    bool isInitPos = false;
                    foreach (Vector2Int pos in matchPos)
                    {
                        if (x == pos.x && y == pos.y)
                        {
                            isInitPos = true;
                        }
                    }
                    if (isInitPos)
                    {
                        Assert.IsTrue(match, string.Format("Match at {0}, {1}", x, y));
                    }
                    else
                    {
                        Assert.IsFalse(match, string.Format("Match at {0}, {1}", x, y));
                    }
                }
            }

        }

        static void TestGetAllMatch()
        {
            Debug.Log("TestGetAllMatch");

            Match3TileGrid grid = new Match3TileGrid(8, 8);
            grid.SetTileTypeByCellPos(1, 1, 1);
            grid.SetTileTypeByCellPos(1, 2, 1);
            grid.SetTileTypeByCellPos(1, 3, 1);
            grid.SetTileTypeByCellPos(1, 4, 1);

            grid.SetTileTypeByCellPos(2, 1, 3);
            grid.SetTileTypeByCellPos(2, 1, 4);
            grid.SetTileTypeByCellPos(2, 1, 5);
            grid.SetTileTypeByCellPos(2, 1, 6);

            grid.SetTileTypeByCellPos(3, 4, 3);
            grid.SetTileTypeByCellPos(3, 4, 4);
            grid.SetTileTypeByCellPos(3, 4, 5);

            grid.SetTileTypeByCellPos(4, 2, 6);
            grid.SetTileTypeByCellPos(4, 3, 6);
            grid.SetTileTypeByCellPos(4, 4, 6);

            List<Match3Tile> matchedTilesSingle = new List<Match3Tile>();
            for (int y = 0; y < grid.size.y; y++)
            {
                for (int x = 0; x < grid.size.x; x++)
                {
                    if (grid.TestMatch3(x, y))
                    {
                        matchedTilesSingle.Add(grid.GetTileByCellPos(x, y));
                    }
                }
            }
            var matchedTiles = grid.GetAllMatches();
            Assert.AreEqual(matchedTilesSingle.Count, matchedTiles.Count);

            foreach (TileData tile in matchedTiles)
            {
                Assert.IsNotNull(matchedTilesSingle.Find(x => x.pos.x == tile.cellPos.x && x.pos.y == tile.cellPos.y),
                    string.Format("Matched Tile at {0}, {1}", tile.cellPos.x, tile.cellPos.y));
            }
        }

        static void TestSwitchTiles()
        {
            Debug.Log("TestSwitchTiles");
            TileGeneration.RandomTileGenerator tg = new TileGeneration.RandomTileGenerator();
            tg.SetTileTypes(new int[] { 1, 2, 3, 4, 5});

            for (int i = 0; i < 1000; i++)
            {
                Match3TileGrid grid = new Match3TileGrid(3, 3);
                tg.Fill(grid);

                int typeA = grid.GetTileTypeByCellPos(1, 1);
                int typeB = grid.GetTileTypeByCellPos(2, 1);

                grid.SwitchTilesByCellPos(1, 1, 2, 1);

                Assert.AreNotEqual(Constants.INVALID_ID, grid.GetTileTypeByCellPos(2, 1));
                Assert.AreNotEqual(Constants.INVALID_ID, grid.GetTileTypeByCellPos(2, 1));
                Assert.AreEqual(typeA, grid.GetTileTypeByCellPos(2, 1));
                Assert.AreEqual(typeB, grid.GetTileTypeByCellPos(1, 1));
            }
        }
    }
}
