﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;
using BGF.Core;
using BGF.Core.TileGeneration;

namespace BGF.Core.UnitTest
{

    public class RandomTileGeneratorUnitTest
    {
        [MenuItem("BGF/Unit Test/Core/Tile Generation/Random Tile Generator")]
        static void UnitTest()
        {
            Debug.Log("Unit Test - Random Tile Generator");
            TestFillGrid();
            TestExcludedRandomType();
            TestNoReadyMatchAtInit();
            Debug.Log("Unit Test End - Random Tile Generator");
        }

        static void TestFillGrid()
        {
            Debug.Log("TestFillGrid");
            RandomTileGenerator tg = new RandomTileGenerator();
            int type = tg.GetRandomTileType();
            Assert.AreEqual<int>(Constants.INVALID_ID, type, "Test Empty Type Set");

            var newTypeSet = new int[] { 1, 2, 3 };
            tg.SetTileTypes(newTypeSet);
            for (int i = 0; i < 5000; i++)
            {
                type = tg.GetRandomTileType();
                Assert.AreNotEqual<int>(Constants.INVALID_ID, type, "Test Random Tile Type");
            }

            Match3TileGrid grid = new Match3TileGrid(5, 5);
            Assert.AreEqual<int>(0, grid.GetTiles(x => x.typeId != Constants.INVALID_ID).Count, "Test Empty Grid");

            tg.Fill(grid);
            Assert.AreEqual<int>(0, grid.GetTiles(x => x.typeId == Constants.INVALID_ID).Count, "Test Fill Grid");
        }

        static void TestExcludedRandomType()
        {
            Debug.Log("TestExcludedRandomType");
            RandomTileGenerator tg = new RandomTileGenerator();
            var newTypeSet = new int[] { 1, 2, 3 };
            tg.SetTileTypes(newTypeSet);

            for (int i = 0; i < 5000; i++)
            {
                int type = tg.GetRandomTileTypeExcept(new int[] { 2, 3 });
                Assert.AreEqual<int>(1, type);
            }
        }

        static void TestNoReadyMatchAtInit()
        {
            Debug.Log("TestNoReadyMatchAtInit");

            RandomTileGenerator tg = new RandomTileGenerator();
            var newTypeSet = new int[] { 1, 2, 3, 4, 5, 6 };
            tg.SetTileTypes(newTypeSet);

            for (int i = 0; i < 1000; i++)
            {
                Match3TileGrid grid = new Match3TileGrid(100, 100);
                tg.Fill(grid);
                Assert.AreEqual<int>(0, grid.GetAllMatches().Count, "Matched Tile Count");
            }
            
        }
    }
}