﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGF.UI.Views
{
    public abstract class BaseView : MonoBehaviour
    {
        protected abstract void InitView();

        public static T CreateObject<T>(Transform aNewParent) where T : BaseView
        {
            GameObject obj = new GameObject();
            T view = obj.AddComponent<T>();
            obj.transform.SetParent(aNewParent);
            obj.transform.localScale = Vector3.one;
            obj.name = view.GetType().Name;
            return view;
        }

        public virtual void Refresh()
        { }
    }
}