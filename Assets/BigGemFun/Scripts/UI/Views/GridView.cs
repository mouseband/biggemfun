﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using BGF.Core;
using BGF.UI.ViewModels;
using BGF.Game.Assets;
using BGF.Core.TileGeneration;

namespace BGF.UI.Views
{
    public abstract class GridView : BaseView
    {
        public abstract Vector3 CellPosToRealPos(Vector2Int cellPos);
        protected abstract TileView AddTileView(TileData data);

        #region Properties
        public Vector2Int size
        {
            get
            {
                if (null == viewModel)
                {
                    return new Vector2Int();
                }
                return viewModel.size;
            }
        }

        public GridViewModel viewModel { get; protected set; }

        public bool isPorcessing { get; protected set; }

        Dictionary<int, TileView> tileViews = new Dictionary<int, TileView>();

        Match3TileMessagePackage messageQueue = new Match3TileMessagePackage();

        List<int> moveInTilePosList = new List<int>();
        List<TileView> readyToMoveInTiles = new List<TileView>();

        public bool enableSound = true;

        [SerializeField]
        protected TileView tileViewTemplate;
        #endregion Properties

        #region Actions
        public void AddTiles(List<TileData> newTiles)
        {
            if (null == newTiles)
            {
                return;
            }

            foreach (TileData data in newTiles)
            {
                TileView tileView = CreateTileView(data);
                PutTileViewIntoMoveInCellPos(tileView);
            }
        }

        public IEnumerator SwitchTiles(TileView from, TileView to, System.Action OnDone)
        {

            if (null == from || null == to)
            {
                yield break;
            }
            isPorcessing = true;
            int count = 2;

            Vector2Int fromPos = from.pos;
            Vector2Int toPos = to.pos;

            from.StartCoroutine(from.MoveToCell(toPos.x, toPos.y, (tv) => { --count; }));
            to.StartCoroutine(to.MoveToCell(fromPos.x, fromPos.y, (tv) => { --count; }));
            yield return new WaitUntil(() => 0 >= count);
            isPorcessing = false;
            OnDone?.Invoke();
            
                 
        }

        public IEnumerator MoveTiles(List<TileData> movedTiles)
        {
            int count = 0;
            if (null != movedTiles)
            {
                // Move tiles from list
                foreach (TileData data in movedTiles)
                {
                    if (null == data)
                    {
                        continue;
                    }
                    TileView tileView = GetTileViewById(data.id);
                    if (null == tileView)
                    {
                        continue;
                    }
                    ++count;
                    tileView.StartCoroutine(tileView.MoveToCell(data.cellPos.x, data.cellPos.y, (tv) => { --count; }));
                }
            }

            // Also move tiles at move in pos
            foreach (TileView tileView in readyToMoveInTiles)
            {
                if (null == tileView)
                {
                    continue;
                }
                ++count;
                tileView.StartCoroutine(tileView.MoveToCell(tileView.pos.x, tileView.pos.y, (tv) => { --count; }));
            }

            ResetMoveInPosTileList();
            readyToMoveInTiles.Clear();
            yield return new WaitUntil(() => 0 >= count);
        }

        void PutTileViewIntoMoveInCellPos(TileView tile)
        {
            if (null == tile)
            {
                return;
            }

            try
            {
                tile.SetToCell(tile.pos.x, moveInTilePosList[tile.pos.x], false);
                moveInTilePosList[tile.pos.x]--;
                readyToMoveInTiles.Add(tile);
            }
            catch { }
        }

        void ResetMoveInPosTileList()
        {
            moveInTilePosList.Clear();
            for (int i = 0; i < size.x; i++)
            {
                moveInTilePosList.Add(-1);
            }
        }

        public IEnumerator PopTiles(List<TileData> tileList)
        {
            int count = 0;
            foreach (TileData data in tileList)
            {
                if (null == data)
                {
                    continue;
                }
                TileView tileView = GetTileViewById(data.id);
                if (null == tileView)
                {
                    continue;
                }
                ++count;
                StartCoroutine(tileView.PopTile((tv) => { --count; }));
            }


            yield return new WaitUntil(() => 0 >= count);
        }

        public IEnumerator PopAllTiles()
        {
            List<TileData> tileList = new List<TileData>();
            foreach (TileView tileView in tileViews.Values)
            {
                tileList.Add(tileView.ToTileData());
            }
            bool originEnableSound = enableSound;
            enableSound = false;
            yield return StartCoroutine(PopTiles(tileList));
            enableSound = originEnableSound;
        }
        #endregion Actions

        #region Messages

        public void AddPackage(Match3TileMessagePackage package)
        {
            messageQueue.AddPackage(package);
        }

        IEnumerator ProcessMessage()
        {
            while (true)
            {
                isPorcessing = 0 < messageQueue.Count;
                while (0 < messageQueue.Count)
                {
                    Match3TileMessage message = messageQueue.DequeueMessage();
                    switch (message.id)
                    {
                        case Match3TileBoard.ACT_ADD_TILES_KEY:
                            AddTiles(message.data);
                            break;
                        case Match3TileBoard.ACT_MOVE_TILES_KEY:
                            yield return StartCoroutine(MoveTiles(message.data));
                            break;
                        case Match3TileBoard.ACT_POP_TILES_KEY:
                            BGFGame.OnPopTiles(message.data);
                            yield return StartCoroutine(PopTiles(message.data));
                            break;
                        case Match3TileBoard.ACT_SHUFFLE_KEY:
                            yield return StartCoroutine(PopAllTiles());
                            break;
                        case Match3TileBoard.ACT_SHOW_TIP_KEY:
                            yield return StartCoroutine(ShowTips(message.data));
                            break;
                        case Match3TileBoard.ACT_MOVE_COUNT_KEY:
                            BGFGame.OnMove();
                            break;
                    }
                }
                yield return null;
            }
        }

        public IEnumerator ShowTips(List<TileData> keyMoves)
        {
            HashSet<int> ids = new HashSet<int>();
            int count = 0;
            foreach (TileData data in keyMoves)
            {
                if (null == data || ids.Contains(data.id))
                {
                    continue;
                }
                ids.Add(data.id);
                ++count;
                TileView tileView = GetTileViewById(data.id);
                StartCoroutine(tileView.ShowTip((tv) => { --count; }));
            }
            yield return new WaitUntil(() => 0 >= count);
        }
        #endregion Messages

        #region Init
        public void Setup(int sizeX, int sizeY, Match3TileGernerator generator, IEnumerable<int> tileTypeSet)
        {
            SetupViewModel(sizeX, sizeY, generator, tileTypeSet);
            ResetMoveInPosTileList();
            InitView();
            StartCoroutine(ProcessMessage());
        }

        protected virtual void SetupViewModel(int sizeX, int sizeY, Match3TileGernerator generator, IEnumerable<int> tileTypeSet)
        {
            viewModel = new GridViewModel(sizeX, sizeY, this, generator, tileTypeSet);
        }
        #endregion Init

        #region Tile Views

        public TileView GetTileViewById(int tileId)
        {
            if (tileViews.ContainsKey(tileId))
            {
                return tileViews[tileId];
            }
            return null;
        }

        public TileView CreateTileView(TileData data)
        {
            if (null == data)
            {
                return null;
            }

            if (tileViews.ContainsKey(data.id))
            {
                Debug.LogErrorFormat("Add Tile View Fail: Already exists tile id {0}", data.id);
                return null;
            }

            TileView tile = AddTileView(data);
            tile.Setup(this, data);
            tileViews.Add(tile.tileId, tile);
            return tile;
        }
        
        public void RemoveTileView(int tileId)
        {
            if (!tileViews.ContainsKey(tileId))
            {
                return;
            }

            RemoveTileView(tileViews[tileId]);
        }

        public void RemoveTileView(TileView tileView)
        {
            if (null == tileView)
            {
                return;
            }
            int tileId = tileView.tileId;

            if (tileViews.ContainsKey(tileId))
            {
                tileViews.Remove(tileId);
            }
        }

        public TileView GetTileByPos(int posX, int posY)
        {
            try
            {
                return tileViews.Values.ToList<TileView>().Find(x => x.pos.x == posX && x.pos.y == posY);
            }
            catch { }
            return null;
        }
        #endregion Tile Views
    }
}
