﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BGF.Core;
using BGF.UI.ViewModels;

namespace BGF.UI.Views
{
    public abstract class TileView : BaseView
    {
        protected abstract void SetToPos(Vector3 pos);

        protected abstract IEnumerator MoveToPos(Vector3 pos);

        public int tileTypeId { get; protected set; }
        public int tileId { get; protected set; }
        public Vector2Int pos { get; set; }

        public GridView gridView { get; protected set; }

        public GridViewModel viewModel
        {
            get
            {
                return null == gridView ? null : gridView.viewModel;
            }
        }

        #region Actions
        public void SetToCell(int x, int y, bool updateCellPosToTile)
        {
            Vector3 targetPos = gridView.CellPosToRealPos(new Vector2Int(x, y));
            if (updateCellPosToTile)
            {
                pos = new Vector2Int(x, y);
            }
            SetToPos(targetPos);
        }

        public IEnumerator MoveToCell(int x, int y, System.Action<TileView> OnDone)
        {
            Vector3 targetPos = gridView.CellPosToRealPos(new Vector2Int(x, y));
            yield return StartCoroutine(MoveToPos(targetPos));
            pos = new Vector2Int(x, y);
            OnDone?.Invoke(this);
        }

        public IEnumerator PopTile(System.Action<TileView> OnDone)
        {
            gridView.RemoveTileView(this);
            yield return StartCoroutine(OnPopTile());
            OnDone?.Invoke(this);
            Destroy(gameObject);
        }

        protected virtual IEnumerator OnPopTile()
        {
            yield return null;
        }

        public IEnumerator ShowTip(System.Action<TileView> OnDone)
        {
            yield return StartCoroutine(OnShowTip());
            OnDone?.Invoke(this);
        }

        protected virtual IEnumerator OnShowTip()
        {
            yield return null;
        }

        #endregion Actions

        #region Setup
        public void Setup(GridView aNewGridView, TileData data)
        {
            gridView = aNewGridView;

            tileTypeId = data.typeId;
            tileId = data.id;
            pos = data.cellPos;
            SetToCell(pos.x, pos.y, false);
            InitView();
        }
        #endregion

        #region Helper

        protected IEnumerator WaitOneFrameWithPause()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitUntil(()=>!BGF.BGFGame.pause);
        }

        public TileData ToTileData()
        {
            TileData data = new TileData();
            data.id = this.tileId;
            data.typeId = this.tileTypeId;

            data.cellPos = this.pos;
            return data;
        }

        public override string ToString()
        {
            return string.Format("[TileView - id: {0}, type: {1}, pos: {2}, {3} ]", tileId, tileTypeId, pos.x, pos.y);
        }
        #endregion Helper
    }
}