﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BGF.Core;
using BGF.Game.Assets;

namespace BGF.UI.Views
{
    [RequireComponent(typeof(RectTransform))]
    public class ImageGridView : GridView
    {
        public bool autoPlayAtStart = false;

        bool _autoPlay;
        public bool autoPlay
        {
            get
            {
                return _autoPlay;
            }
            set
            {
                _autoPlay = value;
                StopCoroutine("AutoPlay");
                if (_autoPlay)
                {
                    StartCoroutine("AutoPlay");
                    enableSound = false;
                }
            }
        }
        RectTransform rt;

        [SerializeField]
        public ImageTileAssets tileAssetes { get; protected set; }

        protected Vector2Int tileSize;

        protected virtual void Awake()
        {
            rt = GetComponent<RectTransform>();
            BGFGame.gridView = this;
            tileAssetes = LoadTileAssets();
        }

        protected virtual IEnumerator Start()
        {
            // wait 1 frame to refresh UI
            yield return null;
            Setup(9, 9, new BGF.Core.TileGeneration.RandomTileGenerator(), tileAssetes.GetTileTypeSet());
            RefreshTileSize();

            BGFGame.RestartGame();

            autoPlay = autoPlayAtStart;
        }

        IEnumerator AutoPlay()
        {
            while (true)
            {
                yield return new WaitUntil(() => !isPorcessing);
                var matchedMovesMsg = viewModel.gameBoard.GetMatchMoves();
                try
                {
                    TileView A =  GetTileViewById(matchedMovesMsg.data[matchedMovesMsg.data.Count - 2].id);
                    TileView B = GetTileViewById(matchedMovesMsg.data[matchedMovesMsg.data.Count - 1].id);
                    viewModel.SelectTile(A);
                    viewModel.SelectTile(B);
                    }
                catch { }
                yield return new WaitUntil(() => !isPorcessing);
                yield return new WaitForSeconds(1.0f);
            }
        }

        ImageTileAssets LoadTileAssets()
        {
            return Resources.Load<ImageTileAssets>(BGFGame.tileSetAssetsName);
        }

        protected void RefreshTileSize()
        {
            Vector2 gridUISize = GetComponent<RectTransform>().rect.size;
            int minLength = Mathf.Min(Mathf.RoundToInt(gridUISize.x / size.x), Mathf.RoundToInt(gridUISize.y / size.y));
            tileSize = new Vector2Int(minLength, minLength);

        }

        public override Vector3 CellPosToRealPos(Vector2Int cellPos)
        {
            float x = tileSize.x * cellPos.x - tileSize.x* size.x * 0.5f + 0.5f * tileSize.x;
            float y = - (tileSize.y * cellPos.y - tileSize.y * size.y * 0.5f + 0.5f * tileSize.y);
            return new Vector3(x, y);
        }

        protected override void InitView()
        {
            
        }

        protected override TileView AddTileView(TileData data)
        {
            if (null == tileViewTemplate)
            {
                tileViewTemplate = ImageTileView.CreateObject<ImageTileView>(this.transform);
            }
            TileView inst = Instantiate<TileView>(tileViewTemplate, this.transform);
            if (null == inst)
            {
                inst = ImageTileView.CreateObject<ImageTileView>(this.transform);
            }

            try
            {
                Sprite sprite = tileAssetes.GetById(data.typeId);
                ImageTileView imageTileView = (ImageTileView)inst;
                imageTileView.SetSprite(sprite, tileSize);
            }
            catch { }
            
            return inst;
        }
    }
}