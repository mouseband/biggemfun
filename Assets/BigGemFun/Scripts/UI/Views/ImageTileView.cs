﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using BGF.Core;
using BGF.UI.ViewModels;


namespace BGF.UI.Views
{
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(EventTrigger))]
    [RequireComponent(typeof(RectTransform))]
    public class ImageTileView : TileView
    {

        Vector2Int tileSize;

        Vector3 dragStartPos;
        bool canSwitch = false;

        public void SetSprite(Sprite sprite, Vector2Int size)
        {
            GetComponent<Image>().sprite = sprite;
            GetComponent<RectTransform>().sizeDelta = size;
            tileSize = size;
        }

        protected override void InitView()
        {
            GetComponent<Button>().enabled = true;
            if (!BGFGame.swipeMode)
            {
                GetComponent<Button>().onClick.AddListener(() => {
                    Debug.Log(this);
                    viewModel.SelectTile(this);
                });
            }
        }

        public void OnDragBegin()
        {
            if (!BGFGame.swipeMode)
            {
                return;
            }
            canSwitch = true;
            dragStartPos = Input.mousePosition;
        }

        public void OnDrag()
        {
            if (!canSwitch)
            {
                return;
            }

            Vector2 diff = Input.mousePosition - dragStartPos;
            float sqrDist = Vector2.SqrMagnitude(diff);

            if (sqrDist > tileSize.x )
            {
                float angle = Mathf.Atan2(diff.y, diff.x);
                angle *= Mathf.Rad2Deg;
                angle += 0 > angle ? 360 : 0;

                if (330 < angle || 30 > angle)
                {
                    ReSelectThis();
                    viewModel.SelectTile(gridView.GetTileByPos(pos.x + 1, pos.y));
                    canSwitch = false;
                }
                else if (60 < angle && angle < 120)
                {
                    ReSelectThis();
                    viewModel.SelectTile(gridView.GetTileByPos(pos.x, pos.y - 1));
                    canSwitch = false;
                }
                else if (150 < angle && angle < 210)
                {
                    ReSelectThis();
                    viewModel.SelectTile(gridView.GetTileByPos(pos.x - 1, pos.y));
                    canSwitch = false;
                }
                else if (240 < angle && angle < 300)
                {
                    ReSelectThis();
                    viewModel.SelectTile(gridView.GetTileByPos(pos.x, pos.y + 1));
                    canSwitch = false;
                }
            }
        }

        void ReSelectThis()
        {
            viewModel.ResetSelectedTile();
            viewModel.SelectTile(this);
        }

        protected override void SetToPos(Vector3 pos)
        {
            RectTransform rt = GetComponent<RectTransform>();
            rt.localPosition = pos;
        }

        protected override IEnumerator OnPopTile()
        {
            float step = 0;
            Image img = GetComponent<Image>();
            float initAlpha = img.color.a;

            if (gridView.enableSound)
            {
                Game.GameAudio.PlayPopSFX(tileTypeId);
            }
            
            Vector3 initScale = transform.localScale;
            while (step < 1)
            {
                Color color = img.color;
                step += Time.deltaTime * 5.0f;
                color.a = Mathf.Lerp(initAlpha, 0.0f, step);
                img.color = color;
                transform.localScale = Vector3.Lerp(initScale, initScale * 2.0f, step);

                yield return StartCoroutine(WaitOneFrameWithPause());
            }
        }

        protected override IEnumerator MoveToPos(Vector3 pos)
        {
            RectTransform rt = GetComponent<RectTransform>();
            float step = 0;
            Vector2 oriPos = rt.localPosition;
            int speed = 9;
            float time = Vector2.Distance(oriPos, pos) / (tileSize.y * speed);
            while (step < 1)
            {
                rt.localPosition = Vector2.Lerp(oriPos, pos, step += Time.deltaTime / time);
                yield return StartCoroutine(WaitOneFrameWithPause());
            }
        }

        protected override IEnumerator OnShowTip()
        {
            Vector3 scale = transform.localScale;
            for (int i = 0; i < 3; i++)
            {
                float step = 0.0f;
                while (step < 1)
                {
                    transform.localScale = Vector3.Lerp(scale, Vector3.one * 0.8f, step += Time.deltaTime * 6.0f);
                    yield return StartCoroutine(WaitOneFrameWithPause());
                }

                step = 0.0f;
                while (step < 1)
                {
                    transform.localScale = Vector3.Lerp(Vector3.one * 0.8f, scale, step += Time.deltaTime * 6.0f);
                    yield return StartCoroutine(WaitOneFrameWithPause());
                }
            }
            transform.localScale = scale;
        }
    }
}