﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BGF.Core;
using BGF.Core.TileGeneration;
using BGF.UI.Views;


namespace BGF.UI.ViewModels
{
    public class GridViewModel
    {
        public Match3TileBoard gameBoard { get; protected set; }

        public Vector2Int size
        {
            get
            {
                if (null == gameBoard)
                {
                    return new Vector2Int();
                }
                return gameBoard.size;
            }
        }

        protected GridView gridView;

        public TileView selectedTile { get; protected set; }

        public GridViewModel(int sizeX, int sizeY, GridView aNewGridView, Match3TileGernerator generator, IEnumerable<int> tileTypeSet)
        {
            gameBoard = new Match3TileBoard();
            gameBoard.InitBoard(sizeX, sizeY, generator, tileTypeSet);
            gridView = aNewGridView;
        }

        #region Message Actions
        public void ResetGame()
        {
            Match3TileMessagePackage messagePackage = gameBoard.Shuffle();
            OnReceiveMessagePackage(messagePackage);
        }

        public void ResetSelectedTile()
        {
            selectedTile = null;
        }

        public void SelectTile(TileView tileView)
        {
            // No input while processing
            if (gridView.isPorcessing)
            {
                return;
            }


            if (null == selectedTile)
            {
                selectedTile = tileView;
                return;
            }

            if (null == tileView || selectedTile == tileView)
            {
                return;
            }

            bool isConnectedTile = ((tileView.pos.x == selectedTile.pos.x + 1 || tileView.pos.x == selectedTile.pos.x - 1)
                && tileView.pos.y == selectedTile.pos.y)
                || ((tileView.pos.y == selectedTile.pos.y + 1 || tileView.pos.y == selectedTile.pos.y - 1)
                && tileView.pos.x == selectedTile.pos.x);

            if (!isConnectedTile)
            {
                selectedTile = tileView;
                return;
            }

            TileView toTile = selectedTile;
            gridView.StartCoroutine(gridView.SwitchTiles(toTile, tileView, () => {
                SwitchTiles(toTile.pos, tileView.pos);
            }));

            selectedTile = null;
        }

        public void SwitchTiles(Vector2Int fromCell, Vector2Int toCellPos)
        {
            Match3TileMessagePackage messagePackage = gameBoard.SwitchTiles(fromCell, toCellPos);
            OnReceiveMessagePackage(messagePackage);
        }

        public void ShowTips()
        {
            Match3TileMessagePackage messagePackage = gameBoard.ShowTips();
            OnReceiveMessagePackage(messagePackage);
        }
        #endregion Message Actions

        #region Message Receiver
        protected void OnReceiveMessagePackage(Match3TileMessagePackage package)
        {
            gridView.AddPackage(package);
        }
        #endregion Message Receiver
    }
}