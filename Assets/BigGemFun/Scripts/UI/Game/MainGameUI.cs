﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BGF.Game;

namespace BGF.UI
{
    public class MainGameUI : MonoBehaviour
    {
        [SerializeField]
        PauseMenuUI pauseMenu = null;

        [SerializeField]
        PauseMenuUI gameOverMenu = null;

        [SerializeField]
        Button tipButton = null;

        [SerializeField]
        Text levelTxt = null;

        [SerializeField]
        Text infoText = null;

        [SerializeField]
        AudioClip gameOverSfx = null;

        [SerializeField]
        AudioClip levelUpSfx = null;

        bool showGameOver = false;
        private void Awake()
        {
            BGFGame.mainGameUI = this;

            tipButton.onClick.RemoveAllListeners();
            tipButton.onClick.AddListener(() => {
                BGFGame.gridView.viewModel.ShowTips();
                tipButton.interactable = false;
                Invoke("ResetTipButton", 5.0f);
            });
        }

        void ResetTipButton()
        {
            tipButton.interactable = true;
        }

        public void InitLevel(int level)
        {
            Debug.Log("InitLevel " + level.ToString());
            levelTxt.text = string.Format("Level {0}", level);
            if (level > 1)
            {
                StartCoroutine(ShowText("LEVEL UP", levelUpSfx));
            }
            showGameOver = true;
        }

        IEnumerator ShowText(string text, AudioClip sound)
        {
            infoText.text = text;
            infoText.gameObject.SetActive(true);
            GameAudio.sound.PlayOneShot(sound);
            yield return new WaitForSeconds(2.0f);
            infoText.gameObject.SetActive(false);

        }

        public void GameOver()
        {
            if (!showGameOver)
            {
                return;
            }
            showGameOver = false;
            Debug.Log("GameOver");
            BGFGame.pause = true;
            gameOverMenu.gameObject.SetActive(true);
            StartCoroutine(ShowText("GAME OVER", gameOverSfx));
        }

        public void OnPauseButton()
        {
            BGFGame.pause = true;
            pauseMenu.gameObject.SetActive(true);
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                OnPauseButton();
            }
        }

        private void Update()
        {
            BGFGame.OnUpdate();
        }
    }

}
