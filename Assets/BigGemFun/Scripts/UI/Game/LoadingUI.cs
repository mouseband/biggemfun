﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGF.UI
{ 
    public class LoadingUI : MonoBehaviour
    {
        // Start is called before the first frame update
        IEnumerator Start()
        {
            yield return null;
            yield return new WaitForSeconds(0.5f);
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }
    }
}