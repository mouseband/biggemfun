﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BGF.UI
{
    public class TimerUI : MonoBehaviour
    {
        [SerializeField]
        Image timerImage = null;

        [SerializeField]
        Text timerText = null;

        private void OnEnable()
        {
            gameObject.SetActive(BGFGame.enableTime || BGFGame.enableMoves);
        }

        private void Update()
        {
            if (BGFGame.enableTime && BGFGame.totalTime > 0.0f)
            {
                timerImage.fillAmount = Mathf.Clamp01(BGFGame.time / BGFGame.totalTime);
                timerText.text = Mathf.FloorToInt(BGFGame.time).ToString() + " SEC";
            }
            else if (BGFGame.enableMoves && BGFGame.totalMoves > 0)
            {
                timerImage.fillAmount = Mathf.Clamp01((float)BGFGame.moves / (float)BGFGame.totalMoves);
                timerText.text = BGFGame.moves.ToString() +  " MOVES";
            }
        }
    }
}