﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BGF;
using BGF.Game;

namespace BGF.UI
{
    public class PauseMenuUI : MonoBehaviour
    {
        [SerializeField]
        Toggle musicToggle = null;

        [SerializeField]
        Toggle soundToggle = null;

        private void OnEnable()
        {
            musicToggle.isOn = !GameAudio.music.mute;
            soundToggle.isOn = !GameAudio.sound.mute;
        }

        public void ToggleMusic(bool isOn)
        {
            Debug.Log("ToggleMusic " + isOn.ToString());
            GameAudio.music.mute = !isOn;
        }

        public void ToggleSound(bool isOn)
        {
            Debug.Log("ToggleSound " + isOn.ToString());
            GameAudio.sound.mute = !isOn;
        }

        public void RestartGame()
        {
            Debug.Log("BackToMainMenu");
            BGFGame.RestartGame();
            gameObject.SetActive(false);
            OnResume();
        }

        public void BackToMainMenu()
        {
            Debug.Log("BackToMainMenu");
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }

        public void OnResume()
        {
            BGFGame.pause = false;
        }
    }
}