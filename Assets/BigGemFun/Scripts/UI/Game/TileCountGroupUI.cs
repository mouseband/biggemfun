﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BGF.Core;
using BGF.UI.Views;

namespace BGF.UI
{
    public class TileCountGroupUI : MonoBehaviour
    {
        [SerializeField]
        TileCountItemUI templateItem = null;

        List<TileCountItemUI> items = new List<TileCountItemUI>();

        private void Start()
        {
            InitItems();
        }

        void InitItems()
        {
            ImageGridView imgGridView = (ImageGridView)BGFGame.gridView;
            if (null == imgGridView)
            {
                return;
            }

            for (int i = 0; i < imgGridView.tileAssetes.Count; i++)
            {
                TileCountItemUI item = Instantiate<TileCountItemUI>(templateItem, this.transform);
                item.transform.localScale = Vector3.one;
                item.tileTypeId = i;
                item.gameObject.SetActive(true);
                items.Add(item);
            }
        }

        void RemoveAllItems()
        {
            foreach (TileCountItemUI item in items)
            {
                if (null == item)
                {
                    continue;
                }
                Destroy(item.gameObject);
            }
            items.Clear();
        }
    }
}