﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BGF.UI
{
    public class MainMenuUI : MonoBehaviour
    {
        [SerializeField]
        Button quitButton = null;

        [SerializeField]
        Text versionTxt = null;

        private void OnEnable()
        {
            BGFGame.pause = false;
#if !UNITY_STANDALONE 
            quitButton.gameObject.SetActive(false);
#endif 

            versionTxt.text = "v" + Application.version;
        }
        public void QuitGame()
        {
            Application.Quit();
        }

        public void Play()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");
        }

        public void SetTileType(string name)
        {
            BGFGame.tileSetAssetsName = name;
        }

        public void EnabaleMove()
        {
            BGFGame.enableMoves = true;
            BGFGame.enableTime = false;
        }

        public void EnableTime()
        {
            BGFGame.enableMoves = false;
            BGFGame.enableTime = true;
        }

        public void CasualMode()
        {
            BGFGame.enableMoves = false;
            BGFGame.enableTime = false;
        }

        public void SetSwipe()
        {
            BGFGame.swipeMode = true;
        }

        public void SetTapTap()
        {
            BGFGame.swipeMode = false;
        }
    }
}