﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BGF.Core;
using BGF.UI.Views;

namespace BGF.UI
{
    [RequireComponent(typeof(Image))]
    public class TileCountItemUI : MonoBehaviour
    {
        public int tileTypeId = BGF.Core.Constants.INVALID_ID;

        [SerializeField]
        Text tileCountTxt = null;

        private void Start()
        {
            SetupImage();
        }

        private void Update()
        {
            SetupCountTxt();
        }

        void SetupImage()
        {
            ImageGridView imgGridView = (ImageGridView)BGFGame.gridView;
            if (null != imgGridView)
            {
                GetComponent<Image>().sprite = imgGridView.tileAssetes.GetById(tileTypeId);
            }
        }

        void SetupCountTxt()
        {
            int count = 0;
            try
            {
                count = BGFGame.tileCount[tileTypeId];
            }
            catch { }
            tileCountTxt.text = "x " + count.ToString();
        }
    }
}